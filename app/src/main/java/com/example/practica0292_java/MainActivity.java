package com.example.practica0292_java;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;;

public class MainActivity extends AppCompatActivity {

    // Declaración de componentes del Layout
    private EditText txtAltura;
    private EditText txtPeso;
    private TextView lblIMC;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Relación de los componentes del layout
        txtAltura = (EditText) findViewById(R.id.txtAltura);
        txtPeso = (EditText) findViewById(R.id.txtPeso);
        lblIMC = (TextView) findViewById(R.id.lblIMC);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);


        // Evento clic del Button: btnCalcular
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!txtAltura.getText().toString().equals("") && !txtPeso.getText().toString().equals("")){
                    float altura = Float.parseFloat(txtAltura.getText().toString());
                    float peso = Float.parseFloat(txtPeso.getText().toString());
                    float IMC = peso / (altura*altura);

                    lblIMC.setText("" + Math.round(IMC*100.0)/100.0);
                }
                else{
                    Toast.makeText(MainActivity.this, "Todos los campos son requeridos", Toast.LENGTH_SHORT);
                }

            }
        });


        // Evento clic del Button: btnLimpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                txtAltura.setText("");
                txtPeso.setText("");
                lblIMC.setText("");

            }
        });


        // Evento clic del Button: btnCerrar
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });

    }
}